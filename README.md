# Tight Floe : Running out of Space
### Ludum Dare \#42

Two little penguins share the same egg, and it is a hard time for their father :
there is note enought space on the top of his feets to have them both, and they may die from the frost if they are not kept warm.
Furthemore, when the colony huddles to fight the cold of the floe, the chicks may die under the pressure.
And we also have to feed this little ones.

To make it worse, the floe is slowly melting, and the colony may be taken on an iceberg,
which would make difficult for the female to come back with the food they hunted.
If such a catastrophy were to happend, the colony would have to do their best to leave the twins alive as long as possible.

## Technologies used
- Unity 2018.1.0b13
- Gimp
- Nano
- Vim
- MuseScore
- Audacity