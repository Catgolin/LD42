﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Agent : Interactible {
	public float speed = 1f;
	public abstract void activate();
	public abstract void deactivate();
	public bool alive = true;
	public GameObject walkPrefab;
	public static GameObject ground;

	public void Tick() {
		if(this.alive && this.nbrActions() > 0) {
			if(this.getNextTask().prepare(this)) {
				if(this.getNextTask().performBy(this) != 0) {
					this.endTask();
				}
			} else if(!this.getNextTask().isReachedBy(this)) {
				Debug.Log(this.objectName+" can't do "+this.getNextTask().title()
					+ " on "+this.getNextTask().cible.objectName);
				this.endTask();
			}
		}
	}
	public int nbrActions() {
		return this.transform.GetChild(0).childCount;
	}
	public Action getNextTask() {
		return this.transform.GetChild(0).GetChild(0).GetComponent<Action>();
	}
	public void endTask() {
		GameObject.Destroy(this.transform.GetChild(0).GetChild(0).gameObject);
	}
	public void addWalkTask(Vector2 destination) {
		GameObject action = Instantiate(this.walkPrefab, Vector2.zero, Quaternion.identity);
		action.GetComponent<Walk>().setDestination(destination);
		action.GetComponent<Walk>().cible = Agent.ground.GetComponent<Ground>();
		action.transform.SetParent(this.transform.GetChild(0), false);
	}
	public void addRandomWalkTask() {
		int i = Random.Range(0, Agent.ground.transform.GetChild(1).childCount);
		float x = Agent.ground.transform.GetChild(1).GetChild(i).position.x;
		float y = Agent.ground.transform.GetChild(1).GetChild(i).position.y;
		x += Random.Range(0f, Agent.ground.transform.GetChild(1).GetChild(i).GetComponent<SpriteRenderer>().bounds.extents.x);
		y += Random.Range(0f, Agent.ground.transform.GetChild(1).GetChild(i).GetComponent<SpriteRenderer>().bounds.extents.y);
		this.addWalkTask(new Vector2(x, y));
	}
	public void addAction(GameObject actionPrefab, Interactible cible) {
		Action a = Instantiate(actionPrefab, Vector2.zero, Quaternion.identity).GetComponent<Action>();
		a.cible = cible;
		a.transform.SetParent(this.transform.GetChild(0), false);
	}
}
