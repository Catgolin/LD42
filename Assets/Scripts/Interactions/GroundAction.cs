﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GroundAction : Action {
	public Vector2 destination;
	public void setDestination(Vector2 destination) {
		this.destination = destination;
	}
}
