﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Action : MonoBehaviour {
	public abstract string title();
	public abstract string description();
	public Interactible cible;
	public abstract bool isReachedBy(Agent agent);
	public abstract bool prepare(Agent a);
	public abstract int performBy(Agent agent);
}
