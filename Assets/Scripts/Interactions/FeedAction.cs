﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeedAction : Action {
	public override string title() {return "Feed";}
	public override string description() {return "Uses food to fight chick's hunger";}
	public override bool isReachedBy(Agent agent) {
		if(agent.type() != Interactible.PENGUIN_TYPE) return false;
		if(this.cible.type() != Interactible.CHICK_TYPE) return false;
		Penguin a = (Penguin) agent;
		Chick c = (Chick) this.cible;
		if(c.parent != a && c.parent.incubating == c) return false;
		return a.food > 0 && c.hunger > 0;
	}
	public override bool prepare(Agent a) {
		if(!this.isReachedBy(a)) return false;
		if(Vector2.Distance(a.transform.position, this.cible.transform.position) > 0.5) {
			a.transform.position = Vector2.MoveTowards(
				a.transform.position,
				this.cible.transform.position,
				a.speed * Time.deltaTime);
		}
		if(((Chick)(this.cible)).parent == a && ((Chick)(this.cible)).parent.incubating == this.cible)
			((Penguin)a).freeLegs();
		return Vector2.Distance(a.transform.position, this.cible.transform.position) <= 0.5;
	}
	public override int performBy(Agent agent) {
		if(!this.isReachedBy(agent)) return -1;
		Penguin a = (Penguin) agent;
		a.freeLegs();
		Chick c = (Chick) cible;
		a.feed(c);
		return 0;
	}
}
