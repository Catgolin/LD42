﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeChick : Action {
	public override string title() {return "Free";}
	public override string description() {
		return "The chick you have on your feet is warm near you, but he may have to do some things on his own";
	}
	public override bool isReachedBy(Agent agent) {
		if(this.cible.type() != Interactible.PENGUIN_TYPE) return false;
		if(agent.type() != Interactible.PENGUIN_TYPE || agent != (Agent)this.cible) return false;
		return ((Penguin)agent).incubating != null;
	}
	public override bool prepare(Agent a) { return this.isReachedBy(a); }
	public override int performBy(Agent a) {
		if(!this.isReachedBy(a)) return -1;
		((Penguin)a).freeLegs();
		return 1;
	}
}
