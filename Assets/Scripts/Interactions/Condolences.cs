﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Condolences : Action {
	public override string title() {return "Offer condolences";}
	public override string description() {return "Pay respects to a penguin who lost his chick";}
	public override bool isReachedBy(Agent a) {
		if(a.type() != Interactible.PENGUIN_TYPE) return false;
		if(this.cible.type() == Interactible.PENGUIN_TYPE && ((Penguin)this.cible).mourning > 0 && this.cible != a) {
			foreach(Chick c in this.cible.transform.parent.GetComponent<Familly>().petits)
				if(!c.alive) return true;
		}
		return false;
	}
	public override bool prepare(Agent a) {
		if(Vector2.Distance(a.transform.position, this.cible.transform.position) > 2)
			a.transform.position = Vector2.MoveTowards(
				a.transform.position,
				this.cible.transform.position,
				a.speed * Time.deltaTime);
		return Vector2.Distance(a.transform.position, this.cible.transform.position) <= 2;
	}
	public override int performBy(Agent a) {
		if(this.isReachedBy(a)) {
			((Penguin)this.cible).mourning -= 1;
			return 1;
		}
		return -1;
	}
}
