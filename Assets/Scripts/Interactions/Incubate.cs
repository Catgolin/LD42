﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Incubate : Action {
	public override string title() {return "Keep warm";}
	public override string description() {
		return "Penguins keep their chicks on their feets to keep them warm, but there can only be one at a time";
	}
	public override bool isReachedBy(Agent agent) {
		if(agent.type() != Interactible.PENGUIN_TYPE) return false;
		if(this.cible.type() != Interactible.CHICK_TYPE) return false;
		if((Penguin)agent != ((Chick)this.cible).parent) return false;
		if(((Penguin)agent).incubating == (Chick)this.cible) return false;
		return true;
	}
	public override bool prepare(Agent a) {
		if(!this.isReachedBy(a)) return false;
		if(Vector2.Distance(a.transform.position, this.cible.transform.position) > 0.5) {
			a.transform.position = Vector2.MoveTowards(
				a.transform.position,
				this.cible.transform.position,
				a.speed * Time.deltaTime);
		}
		return Vector2.Distance(a.transform.position, this.cible.transform.position) <= 0.5;
	}
	public override int performBy(Agent agent) {
		if(!this.isReachedBy(agent)) return -1;
		Penguin a = (Penguin) agent;
		Chick c = (Chick) this.cible;
		a.freeLegs();
		if(a.incubating == null) {
			Debug.Log(a.objectName+" is warming up "+c.objectName);
			c.transform.SetParent(a.transform, false);
			c.transform.position = a.transform.position
				+ Vector3.forward
				+ (Vector3.right * 0.2f)
				+ (Vector3.down * 0.2f);
			if(c.transform.parent != a.transform) return 0;
			a.incubating = c;
			return 1;
		} else {
			return 0;
		}
	}

}
