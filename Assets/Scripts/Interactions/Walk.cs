﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walk : GroundAction {
	public GameObject prefabDest;
	
	private GameObject destIndicator;

	public override string title() {return "Walk";}
	public override string description() {return "Juste a simple walking... Wich is not an easy task on the ice !";}
	public override bool isReachedBy(Agent agent) {
		return agent.type() != Interactible.CHICK_TYPE || ((Chick)agent).parent.incubating != agent;
	}
	public override bool prepare(Agent a) {
		if(destIndicator == null) {
			this.destIndicator = GameObject.Instantiate(this.prefabDest, this.destination, Quaternion.identity);
			this.destIndicator.GetComponent<SpriteRenderer>().color =
				new Color(255f, 0f, 0f, GameObject.FindWithTag("GameController").GetComponent<Manager>().debugMode ? 1f : 0f);
		}
		return isReachedBy(a);
	}
	public override int performBy(Agent agent) {
		this.destination = this.destIndicator.transform.position;
		agent.transform.position = Vector2.MoveTowards(agent.transform.position, this.destination, agent.speed * Time.deltaTime);
		return Vector2.Distance(agent.transform.position, this.destination) < 0.1 ? 1 : 0;
	}
	void OnDestroy() {
		GameObject.Destroy(this.destIndicator);
	}
}
