﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class Interactible : MonoBehaviour {
	public GameObject[] actions;
	public GameObject prefabMenu;
	public GameObject prefabItemMenu;
	public Canvas canvas;
	public string objectName;
	
	public const int GROUND_TYPE = -1;
	public const int PENGUIN_TYPE = 0;
	public const int CHICK_TYPE = 1;
	protected Manager manager;

	public abstract int type();

	void Start() {
		this.StartI();
	}
	protected void StartI() {
		this.manager = GameObject.FindWithTag("GameController").GetComponent<Manager>();
	}

	public GameObject openMenu(BaseEventData d) {
		Vector2 position;
		PointerEventData data = null;
		if(d != null) {
			data = (PointerEventData) d;
			position = new Vector2(data.position.x+100, data.position.y - 240 - Camera.main.pixelHeight);
		} else {
			position = Camera.main.WorldToScreenPoint(this.transform.position);
			position = new Vector2(position.x + 100, position.y - 240 - Camera.main.pixelHeight);
		}
		GameObject menu = Instantiate(prefabMenu, position, Quaternion.identity);
		menu.transform.SetParent(this.canvas.transform, false);
		menu.transform.GetChild(1).GetChild(2).GetComponent<Button>().onClick.AddListener(this.manager.closeMenu);
		menu.transform.GetChild(1).GetChild(1).GetComponent<Text>().text = this.objectName;
		int n = 0;
		for(int i = 0; i < this.actions.Length; i++) {
			Action a = Instantiate(this.actions[i], Vector2.zero, Quaternion.identity).GetComponent<Action>();
			a.cible = this;
			a.transform.SetParent(menu.transform, false);
			if(a.isReachedBy(this.manager.getActiveAgent())) {
				if(a is GroundAction) {
					((GroundAction)a).destination = Camera.main.ScreenToWorldPoint(data.position);
				}
				GameObject item = Instantiate(prefabItemMenu, new Vector2(5, -15-(25*n)), Quaternion.identity);
				item.transform.SetParent(menu.transform.GetChild(0), false);
				item.transform.GetChild(0).GetComponent<Text>().text = a.title();
				item.GetComponent<Button>().onClick.AddListener(delegate {
					this.manager.addAction(a);
				});
				n++;
			}
		}
		return menu;
	}
}
