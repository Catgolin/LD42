﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chick : Agent {
	public GameObject penguinInfo;
	public int frost = 0;
	public int hunger = 0;
	public int pressure = 0;
	public Penguin parent;

	private Slider frostLevel;
	private Slider hungerLevel;
	private Slider pressureLevel;
	private Image avatar;
	private float tickCooldown = 0;
	private float blackLevel = 1f;
	
	public override int type() {return Interactible.CHICK_TYPE;}

	void Start () {
		this.StartI();
		if(this.penguinInfo != null) {
			this.avatar = this.penguinInfo.transform.GetChild(0).GetComponent<Image>();
			this.frostLevel = this.penguinInfo.transform.GetChild(1).GetChild(0).GetComponent<Slider>();
			this.hungerLevel = this.penguinInfo.transform.GetChild(2).GetChild(0).GetComponent<Slider>();
			this.pressureLevel = this.penguinInfo.transform.GetChild(3).GetChild(0).GetComponent<Slider>();
		} else {
			this.frost = Random.Range(0, 50);
			this.hunger = Random.Range(0, 50);
		}
	}
	
	void Update () {
		if(this.alive) {
			this.tickCooldown -= Time.deltaTime;
			if(this.tickCooldown < 0) {
				if(this.parent.incubating == this) {
					this.frost --;
					this.pressure -= 5;
				} else {
					this.frost += (this.hunger / 10) - (this.pressure / 10);
					this.frost = this.frost < 0 ? 0 : this.frost;
					if(this.manager.modeDerive && this.manager.toUpdate.IndexOf(this.gameObject) == -1) this.frost *= 2;
					this.pressure -= 2;
				}
				this.hunger += (this.frost / 100) + 1;
				this.tickCooldown = 1;
			}
			if(this.hunger >= 200 || this.frost >= 200 || this.pressure >= 200) {
				this.alive = false;
				if(this.penguinInfo != null)
					this.avatar.transform.GetChild(0).GetComponent<Text>().text = this.objectName+" is dead";
				this.manager.die(this);
			}
			if(this.parent.incubating != this && this.nbrActions() < 1) {
				if(this.hunger > 50 || this.frost > 150 || this.pressure > 150)
					this.addWalkTask(this.parent.transform.position);
				else if(this.pressure < 50 || this.frost > (this.pressure / 10)) {
					GameObject[] list = GameObject.FindGameObjectsWithTag("AI");
					Vector3 crechePos = this.transform.position;
					int crecheSize = 1;
					foreach(GameObject g in list) {
						if(g.GetComponent<Chick>() != null
							&& g != this.gameObject
							&& (!this.manager.modeDerive || this.manager.toUpdate.IndexOf(g) != -1)) {
							crechePos += g.transform.position;
							crecheSize ++;
						}
					}
					crechePos /= crecheSize;
					crechePos += Random.insideUnitSphere * ((float)this.pressure)/200f;
					crechePos = new Vector3(crechePos.x, crechePos.y, 0f);
					this.addWalkTask(crechePos);
				} else this.addRandomWalkTask();
			}
			if(this.penguinInfo != null) this.updateUI();
		} else {
			this.gameObject.GetComponent<SpriteRenderer>().color = new Color(this.blackLevel, this.blackLevel, this.blackLevel);
			this.blackLevel -= 0.1f * Time.deltaTime;
		}
	}
	void updateUI() {
		this.frostLevel.value = (float)frost / 200f;
		this.hungerLevel.value = (float)hunger / 200f;
		this.pressureLevel.value = (float)pressure / 200f;
	}
	public override void activate() {
		this.penguinInfo.transform.GetChild(4).GetComponent<Button>().interactable = false;
		this.manager.activate(this);
	}
	public override void deactivate() {
		this.penguinInfo.transform.GetChild(4).GetComponent<Button>().interactable = true;
		this.manager.deactivate(this);
	}
	public void OnTriggerEnter2D(Collider2D c) {
		if(c.gameObject.GetComponent<Interactible>() != null)
			this.pressure += 1;
	}
	public void OnTriggerStay2D(Collider2D c) {
		if(c.gameObject.GetComponent<Interactible>() != null && this.tickCooldown - Time.deltaTime <= 0)
			this.pressure += 1;
	}
}
