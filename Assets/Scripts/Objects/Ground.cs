﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : Interactible {
	void Start () {
		this.StartI();
	}
	public override int type() {
		return Interactible.GROUND_TYPE;
	}
}
