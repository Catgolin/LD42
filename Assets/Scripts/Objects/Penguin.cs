﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Penguin : Agent {
	public GameObject penguinInfo;
	public AudioSource mournCry;

	public Chick incubating = null;
	public int food = 2000;
	public float mourning;
	
	private Slider foodLevel;
	private float tick = 1;
	private bool blue = false;
	private float blackLevel = 1f;
	public override int type() {return Interactible.PENGUIN_TYPE;}
	// Use this for initialization
	void Start () {
		this.StartI();
		if(this.penguinInfo != null) 
			this.foodLevel = this.penguinInfo.transform.GetChild(1).GetChild(0).GetComponent<Slider>();
		this.mourning = (float) this.transform.parent.parent.childCount;
	}
	public void mourn() {
		this.mournCry.Play();
		this.gameObject.GetComponent<SpriteRenderer>().color = blue ? Color.blue : Color.white;
		this.blue = ! this.blue;
	}
	// Update is called once per frame
	void LateUpdate() {
		if(this.mourning <= 0) {
			this.gameObject.GetComponent<SpriteRenderer>().color = new Color(this.blackLevel, this.blackLevel, this.blackLevel);
			this.blackLevel -= 0.3f * Time.deltaTime;
		}
		if(this.penguinInfo != null) this.updateUI();
		this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 1);
		this.speed = this.incubating == null ? 1f : 0.5f;
		this.tick -= Time.deltaTime;
		if(this.penguinInfo == null && this.nbrActions() == 0 && this.tick <= 0) {
			this.tick = 1;
			Chick petit;
			if(this.transform.parent.childCount == 2) {
				petit = this.transform.parent.GetChild(1).GetComponent<Chick>();
				if(!petit.alive && this.mourning > 0) {
					this.mourning -= Time.deltaTime;
					this.mourn();
				}
				if((petit.alive || this.mourning <= 0) && this.manager.toUpdate.IndexOf(this.gameObject) == -1)
					this.addRandomWalkTask();
				else if(petit.alive && petit.hunger > 150) this.addAction(petit.actions[0], petit);
				else if(petit.alive && petit.frost > 100) this.addAction(petit.actions[1], petit);
				else if(petit.alive && petit.pressure > 100) this.addAction(petit.actions[1], petit);
			} else if(this.transform.childCount == 2) {
				petit = this.transform.GetChild(1).GetComponent<Chick>();
				if(!petit.alive) this.mourning -= Time.deltaTime;
				if((petit.alive || this.mourning <= 0) && this.manager.toUpdate.IndexOf(this.gameObject) == -1)
				this.addRandomWalkTask();
				else if(petit.alive && petit.frost < 100 && petit.hunger > 25) this.addAction(this.actions[0], this);
				else if(petit.alive && petit.frost < 1) this.addAction(this.actions[0], this);
			}
			if(this.nbrActions() == 0) {
				// Take care of others
				for(int i = 0; i < this.transform.parent.parent.childCount && this.nbrActions() == 0; i++) {
					if(this.transform.parent.parent.GetChild(i) == this.transform.parent) continue;
					Familly f = this.transform.parent.parent.GetChild(i).gameObject.GetComponent<Familly>();
					for(int j = 0; j < f.petits.Length && this.nbrActions() == 0; j++) {
						if(!f.petits[j].alive && f.petits[j].parent.mourning > 0
							&& (!this.manager.modeDerive
								|| this.manager.toUpdate.IndexOf(f.petits[j].parent.gameObject) != -1))
							this.addAction(this.actions[1],
								f.petits[j].parent);
						else if(f.petits[j].parent.incubating == f.petits[j]) continue;
						else if(f.petits[j].alive && f.petits[j].hunger > 100)
							this.addAction(f.petits[j].actions[0], f.petits[j]);
					}
				}
			}
			if(this.nbrActions() == 0) {
				// Go near other penguins
				this.addRandomWalkTask();
			}
		}
	}
	public void feed(Chick chick) {
		if(chick.alive) chick.hunger --;
		this.food --;
	}
	private void updateUI() {
		this.foodLevel.value = (float)food / 2000f;
	}
	public override void activate() {
		this.penguinInfo.transform.GetChild(2).GetComponent<Button>().interactable = false;
		this.manager.activate(this);
	}
	public override void deactivate() {
		this.penguinInfo.transform.GetChild(2).GetComponent<Button>().interactable = true;
		this.manager.deactivate(this);
	}
	public void freeLegs() {
		if(this.incubating != null) {
			this.incubating.transform.SetParent(this.transform.parent, false);
			this.incubating.transform.position = this.transform.position + (Vector3.right * 0.5f);
			this.incubating = null;
		}
	}
}
