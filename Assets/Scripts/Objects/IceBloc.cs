﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceBloc : MonoBehaviour {
	private Vector3 previousPosition;
	private Vector3 velocity;
	private Manager manager;
	private Color test;
	void Start () {
		this.previousPosition = this.transform.position;
		this.manager = GameObject.FindWithTag("GameController").GetComponent<Manager>();
		this.test = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
	}
	public void Update() {
		if(this.manager.debugMode) this.gameObject.GetComponent<SpriteRenderer>().color = this.test;
		this.velocity = this.transform.position - this.previousPosition;
		this.previousPosition = this.transform.position;
	}
	public void OnTriggerStay2D(Collider2D c) {
		if(this.velocity.magnitude != 0 && (c.gameObject.GetComponent<Chick>() == null
			|| c.gameObject.GetComponent<Chick>().parent.incubating != c.gameObject.GetComponent<Chick>()))
			this.manager.addToUpdate(c.gameObject);
	}
}
