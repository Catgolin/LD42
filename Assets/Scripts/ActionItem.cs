﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionItem : MonoBehaviour {
	public Agent acteur;
	public Action action;
	
	private RectTransform rt;
	void Start() {
		this.rt = this.gameObject.GetComponent<RectTransform>();
		this.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = this.action.title()+" "+this.action.cible.objectName;
		this.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(this.destroy);
	}
	void Update() {
		if(this.action == null) {
			Destroy(this.gameObject);
			return;
		}
		int i = 0;
		while(this.acteur.transform.GetChild(0).GetChild(i).GetComponent<Action>() != this.action) {
			i++;
		}
		this.rt.position = new Vector3(Camera.main.pixelWidth -80 - (i * 55), Camera.main.pixelHeight -75, 0);
	}
	void destroy() {
		GameObject.Destroy(this.action.gameObject);
	}
}
