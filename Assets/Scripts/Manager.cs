﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Manager : MonoBehaviour {
	public bool debugMode;
	public Interactible ground;
	public GameObject prefabActionItem;

	private Agent[] players;
	private Agent[] ais;
	public Agent activeAgent;
	public ArrayList toUpdate;
 
	private GameObject menu;
	public bool modeDerive = false;
	// Use this for initialization
	void Start () {
		this.toUpdate = new ArrayList();
		Agent.ground = this.ground.gameObject;
		GameObject[] p = GameObject.FindGameObjectsWithTag("Player");
		this.players = new Agent[p.Length];
		for(int i = 0; i < p.Length; i++) {
			this.players[i] = p[i].GetComponent<Agent>();
		}
		GameObject[] a = GameObject.FindGameObjectsWithTag("AI");
		this.ais = new Agent[a.Length];
		for(int i = 0; i < a.Length; i++) {
			this.ais[i] = a[i].GetComponent<Agent>();
		}
	}
	
	public Agent getActiveAgent() { return this.activeAgent; }
	
	void FixedUpdate() {
		this.toUpdate.Clear();
	}
	void Update () {
		for(int i = 0; i < this.players.Length; i++) {
			this.players[i].Tick();
		}
		for(int i = 0; i < this.ais.Length; i++) {
			this.ais[i].Tick();
		}
		if(!this.modeDerive && Random.Range(0f, 1f) < 0.001f) this.modeDerive = true;
		if(this.modeDerive) {
			for(int i = 0; i < this.ground.transform.GetChild(1).childCount; i++) {
				this.ground.transform.GetChild(1).GetChild(i).position = 
					this.ground.transform.GetChild(1).GetChild(i).position + (0.25f * Vector3.down * Time.deltaTime);
			}
			foreach(GameObject g in this.toUpdate) {
				g.transform.position = g.transform.position + (0.25f * Vector3.down * Time.deltaTime);
			}
			Camera.main.transform.position = Camera.main.transform.position + (0.25f * Vector3.down * Time.deltaTime);
			if(this.ground.transform.GetChild(1).childCount > 1 && Random.Range(0f, 1f) < 0.001f) {
				this.ground.transform.GetChild(1).GetChild(0).SetParent(this.ground.transform.GetChild(0), false);
			}
		}
	}
	public void addToUpdate(GameObject g) {
		if(this.toUpdate.IndexOf(g) == -1)
			this.toUpdate.Add(g);
	}
	public void activate(Agent a) {
		if(this.activeAgent != null) this.activeAgent.deactivate();
		this.activeAgent = a;
	}
	public void deactivate(Agent a) {
		if(this.activeAgent == a) {
			this.activeAgent = null;
			a.deactivate();
		}
		if(this.menu != null) GameObject.Destroy(this.menu);
	}
	public void OnGroundClick(BaseEventData d) {
		if(this.menu != null) GameObject.Destroy(this.menu);
		if(this.activeAgent != null) this.menu = this.ground.openMenu(d);
	}
	public void openMenu(Interactible i) {
		if(this.menu != null) GameObject.Destroy(this.menu);
		if(this.activeAgent != null) this.menu = i.openMenu(null);
	}
	public void closeMenu() {
		GameObject.Destroy(this.menu);

	}
	public void addAction(Action a) {
		a.transform.SetParent(activeAgent.transform.GetChild(0), false);
		if(this.menu != null) this.closeMenu();
		ActionItem item = Instantiate(this.prefabActionItem, Vector2.zero, Quaternion.identity).GetComponent<ActionItem>();
		item.transform.SetParent(this.activeAgent.canvas.transform, false);
		item.acteur = this.activeAgent;
		item.action = a;
	}
	public void addGroundAction(Action a, PointerEventData d) {
		GroundAction action = (GroundAction) a;
		action.setDestination(Camera.main.ScreenToWorldPoint(d.position));
		this.addAction(a);
	}
	public void die(Chick c) {
		// End of the game
		c.transform.rotation = Quaternion.Euler(0, 0, 90);
	}
}
